package tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import conf.DC;
import conf.Keys;


public class SimpleTest {
	
	@BeforeMethod
	public void setUp(){
		System.setProperty(Keys.chromeCount, "1");
		System.setProperty(Keys.firefoxCount, "1");
	}
	
	@Test
	public void singleNodeTest(){

		DC.setupHubEnvironment();
		Assert.assertEquals(DC.getStartedContainerID().size(), 3,"Continer started by code should be 3 , 1 hub , 1 FF, 1 Chrome");
		DC.cleanHubEnvironment();
		Assert.assertEquals(DC.getStartedContainerID().size(),0);
	}
	
	@Test
	public void multiNodeTestChrome(){
		System.setProperty(Keys.chromeCount, "2");
		DC.setupHubEnvironment();
		Assert.assertEquals(DC.getStartedContainerID().size(), 4,"Continer started by code should be 4 , 1 hub , 1 FF, 2 Chrome");
		DC.cleanHubEnvironment();
		Assert.assertEquals(DC.getStartedContainerID().size(),0);
	}
	
	@Test
	public void multiNodeTestFireFox(){
		System.setProperty(Keys.firefoxCount, "2");
		DC.setupHubEnvironment();
		Assert.assertEquals(DC.getStartedContainerID().size(), 4,"Continer started by code should be 3 , 1 hub , 2 FF, 1 Chrome");
		DC.cleanHubEnvironment();
		Assert.assertEquals(DC.getStartedContainerID().size(),0);
	}

}
