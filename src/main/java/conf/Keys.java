package conf;

//These are the keys that will be used to fetch value from commandLine
//for example -DfirefoxCount=3 etc

public class Keys {
	
	public static final String chromeCount = "chromecount";
	public static final String firefoxCount = "firefoxcount";
	public static final String doc_url = "dockerurl";
	public static final String chromerepo = "chromerepo";
	public static final String firefoxrepo = "firefoxrepo";
	public static final String hubRepo = "hubrepo";
	public static final String pullImgFlag = "pulldocimageflag";

	

}
