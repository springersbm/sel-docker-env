package conf;
import org.apache.commons.lang.StringUtils;

//Lot of improvement required currently the class will check the system property everytime we make a call 
//need to make it more cleaner 

public class Configuration {
	
	private int chromeNodeCount = 1;
	private int firefoxNodeCount = 1;
	private String dockerUrl = "unix:///var/run/docker.sock";;
	private String chrome_repo = "selenium/node-chrome-debug:2.46.0" ;
	private String firefox_repo = "selenium/node-firefox:latest";
	private String hubrepo = "selenium/hub";
	private String hubName = "selenium-hub";
	private boolean pullImageCheck = true;
	
	private static final Configuration instance = new Configuration();
	private Configuration(){
		
	}
	
	public static Configuration getInstance(){
		return instance;
	}
	
	public String getHubRepo(){
		if(StringUtils.isNotBlank(System.getProperty(Keys.hubRepo))){
			hubrepo = System.getProperty(Keys.hubRepo);
		}
		return hubrepo;
	}
	
	public int getChromeNodeCount() {
		if(StringUtils.isNotBlank(System.getProperty(Keys.chromeCount))){
			chromeNodeCount = Integer.valueOf(System.getProperty(Keys.chromeCount));
		}
		return chromeNodeCount;
	}
	
	public int getFirefoxNodeCount() {
		if(StringUtils.isNotBlank(System.getProperty(Keys.firefoxCount))){
			firefoxNodeCount = Integer.valueOf(System.getProperty(Keys.firefoxCount));
		}
		return firefoxNodeCount;
	}
	
	public String getDockerUrl() {
		if(StringUtils.isNotBlank(System.getProperty(Keys.doc_url))){
			dockerUrl = System.getProperty(Keys.doc_url);
		}
		return dockerUrl;
	}
	
	public String getChrome_repo() {
	
		if(StringUtils.isNotBlank(System.getProperty(Keys.chromerepo))){
			chrome_repo = System.getProperty(Keys.chromerepo);
		}
		return chrome_repo;
	}

	public String getFirefox_repo() {
		if(StringUtils.isNotBlank(System.getProperty(Keys.firefoxrepo))){
			firefox_repo = System.getProperty(Keys.firefoxrepo);
		}
		return firefox_repo;
	}

	public String getHubName() {
		return hubName;
	}
	
	public boolean isPullImageFlag(){
		if(StringUtils.isNotBlank(System.getProperty(Keys.pullImgFlag))){
			pullImageCheck = Boolean.valueOf(System.getProperty(Keys.pullImgFlag));
		}
		return pullImageCheck;
	}

	
	
}
