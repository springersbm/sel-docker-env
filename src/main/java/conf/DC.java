package conf;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Link;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.command.PullImageResultCallback;


public class DC {
	
	private static Configuration conf = Configuration.getInstance();
	private static DockerClient dockerClient = DockerClientBuilder.getInstance(conf.getDockerUrl())
			.build();
	private static final List<String> containerIDs = new ArrayList<String>();
	
	public static void setupHubEnvironment(){
		if(conf.isPullImageFlag())
		pullRequiredImages();
		//create hub container
		try{
		String hubID = createHubContainer().getId();
		//Start hub container
		dockerClient.startContainerCmd(hubID).exec();
		containerIDs.add(hubID);
		setupNodes();
		}catch(Exception e){
			System.out.println("Cleaning the container which all are started in between setup and exception thrown");
			e.printStackTrace();
			cleanHubEnvironment();
		}
		
	}
	
	/**
	 * Return list of container ID which is started by the code
	 * @return
	 */
	public static List<String> getStartedContainerID(){
		return new ArrayList<String>(containerIDs);
	}
	
	public static DockerClient getDockerClient(){
		return dockerClient;
	}
	
	public static void cleanHubEnvironment(){
		for(Iterator<String> iterator = containerIDs.iterator();iterator.hasNext();){
			String containerID = iterator.next();
			dockerClient.stopContainerCmd(containerID).withTimeout(2).exec();
			dockerClient.removeContainerCmd(containerID).exec();
			iterator.remove();
		}
	}
	
	private static void setupNodes(){
		//for chrome
		for (int i = 0; i < conf.getChromeNodeCount(); i++) {
			String nodeID = createNodeContainer(
					conf.getChrome_repo()).getId();
			containerIDs.add(nodeID);
			dockerClient.startContainerCmd(nodeID).exec();
		}
		//for firefox
		for (int i = 0; i < conf.getFirefoxNodeCount(); i++) {
			String nodeID = createNodeContainer(
					conf.getFirefox_repo()).getId();
			containerIDs.add(nodeID);
			dockerClient.startContainerCmd(nodeID).exec();
		}
	}
	
	
	private static void pullRequiredImages() {
		dockerClient.pullImageCmd(conf.getHubRepo()).exec(new PullImageResultCallback()).awaitSuccess();
		//Check if chrome images is required or not
		if (conf.getChromeNodeCount() > 0)
			dockerClient.pullImageCmd(conf.getChrome_repo()).exec(new PullImageResultCallback()).awaitSuccess();
		if (conf.getFirefoxNodeCount() > 0)
			dockerClient.pullImageCmd(conf.getFirefox_repo()).exec(new PullImageResultCallback()).awaitSuccess();
	}
	
	private static CreateContainerResponse createHubContainer() {
		removeAllContainerOfImageIfNotUp(conf.getHubRepo());
		ExposedPort tcp22 = ExposedPort.tcp(4444);
		Ports portBinding = new Ports();
		portBinding.bind(tcp22, Ports.Binding(4444));

		return dockerClient.createContainerCmd(conf.getHubRepo()).withExposedPorts(tcp22)
				.withPortBindings(portBinding).withName(conf.getHubName())
				.exec();

	}
	
	private static boolean removeAllContainerOfImageIfNotUp(String imageName) {
		List<Container> runningContainers = dockerClient.listContainersCmd()
				.withShowAll(true).exec();
		//System.out.println(runningContainers);
		for (Container cont : runningContainers) {
			if (imageName.equalsIgnoreCase(cont.getImage())
					&& !cont.getStatus().contains("Up")) {
				System.out.println(cont.getImage());
				dockerClient.removeContainerCmd(cont.getId()).exec();
			}
		}
		return false;
	}
	
	private static CreateContainerResponse createNodeContainer(String repo) {
		removeAllContainerOfImageIfNotUp(repo);
		return dockerClient.createContainerCmd(repo)
				.withLinks(new Link(conf.getHubName(), "hub")).exec();
	}
	
}
